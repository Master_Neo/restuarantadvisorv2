<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//restaurant API
Route::get('Restaurant', 'RestaurantController@index');
Route::get('Restaurant/{id}', 'RestaurantController@show');
Route::put('Restaurant/{id}', 'RestaurantController@update');
Route::delete('Restaurant/{id}', 'RestaurantController@destroy');
Route::post('Restaurant', 'RestaurantController@store');

//admin API route
Route::put('Users', 'RestaurantAdminController@auth_admin');
Route::post('Users', 'RestaurantAdminController@store');

/*
Route::get('RestaurantAdministrator', 'RestaurantAdminController@index');
Route::get('RestaurantAdministrator/{id}', 'RestaurantAdminController@show');
Route::put('RestaurantAdministrator/{id}', 'RestaurantAdminController@update');
Route::delete('RestaurantAdministrator', 'RestaurantAdminController@destroy');


*/




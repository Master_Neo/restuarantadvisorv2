

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RestaurantAdministrator', function (Blueprint $table) {
			
            $table->increments('id');			
            $table->string('password');            
            $table->string('username');            
            $table->string('fullname');
            $table->string('email')->unique();
            $table->Integer('home')			
            $table->Integer('cell')
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RestaurantAdministrator');
    }
}

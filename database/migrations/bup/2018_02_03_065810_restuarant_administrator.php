

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RestuarantAdministrator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RestuarantAdministrator', function (Blueprint $table) {
			
            $table->increments('id');			
            $table->string('password');            
            $table->string('username');            
            $table->string('fullname');
            $table->string('email')->unique();
            $table->Integer('home')->unique();			
            $table->Integer('cell')->unique();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RestuarantAdministrator');
    }
}

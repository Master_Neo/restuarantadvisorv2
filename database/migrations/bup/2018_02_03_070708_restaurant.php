

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use app\Models\RestaurantAdministrator;

class Restaurant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Restaurant', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->Integer('tel');
            $table->double('latitude',15,8);			
            $table->double('longitude',15,8);						
            $table->Integer('restaurant_administrator_Id')->unsigned();
			$table->foreign('restaurant_administrator_Id')->references('id')->on('RestaurantAdministrator');
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Restaurant');
    }
}

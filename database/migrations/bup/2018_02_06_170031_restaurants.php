

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Restaurants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
			$table->string('address');
            $table->Integer('tel');
            $table->double('latitude',15,8);			
            $table->double('longitude',15,8);			
            $table->Integer('RestaurantAdministrators_id');
			$table->foreign('RestaurantAdministrators_id')->references('id')->on('RestaurantAdministrators_id');
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Restaurants');
    }
}

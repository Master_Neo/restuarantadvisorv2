<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RestaurantsAdministrators extends Migration
{

    public function up()
    {
        Schema::create('RestaurantsAdministrators', function (Blueprint $table) {
			
            $table->increments('id');			
            $table->string('password');            
            $table->string('username'); 
			$table->string('address');
            $table->string('fullname');
            $table->string('email');
            $table->Integer('home');		
            $table->Integer('cell');
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RestaurantsAdministrators');
    }
}

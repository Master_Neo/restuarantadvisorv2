<?php

namespace App\Models;

use App\Restuarant_Administrator;
use Illuminate\Http\Request;

class RestaurantAdminController extends Controller
{
	
	public index(){
		
		$restaurantAdministrator = \App\Models\RestaurantAdministrator::with(class::RestaurantAdministrator);
		
		return response()->json([
		"msg"=>"Successfully retrieved restaurant Administrator!",
		"restaurantAdministrator" =>$restaurantAdministrator;
		
		],200
		);
		
		
	}
	
	public show(Resquest $request, $id){
		
		$restaurantAdministrator = \App\Models\RestaurantAdministrator::find($id);
		
		return response()->json([
		"msg"=>"Success!",
		"restaurantAdministrator" => $restaurantAdministrator
		],200
		);		
	
	}
	public store(Request $request){
			
		$restaurantAdministrator =  \App\Models\RestuarantAdministrator();
		
		$cell = $request->cell;		
		$email = $request->email;
		$Address = $request->adrsess;		
		$username  = $request->username;		
		$password  = $request->password;
		$fullname  = $request->fullname;
		
		$restaurantAdministrator->save();
		
		return response()->json([
		"msg"=>"Successfully saved restaurant Administrator!"
		],200
		);
	}
	
	public update(Request $request, $id){
		
		$restuarantAdministrator =  \App\Models\RestuarantAdministrator::find($id);
		
		$fullname  = $request->fullname;
		$username  = $request->username;		
		$password  = $request->password; 
		$Address = $request->address;
		$email = $request->email;
		$cell = $request->cell;
		
		$restuarantAdministrator->save();
		
		return response()->json([
		"msg"=>"Successfully saved restaurant admin!",
		"restuarantAdministrator" => $restuarantAdministrator
		],200
		);	
	}
	
	public destroy(Request $request, $id){
		
		$restuarantAdministrator = \App\RestuarantAdministrator::find($id);
		
		$restuarantAdministrator->delete();
		return response()->json([
		"msg"=>"Successfully deleted restaurant admin!"
		],200
		);	
	}
	

	public auth_admin(Request $request){
		
		$restuarants = \App\Models\RestuarantAdministrator();
		
		$credentials = {password:$request->password,username:$request->username};
		
		if(Auth_Attempt($credentials){
			return response()->json([
		"msg"=>"Successfully authorised in restaurant admin!",
		"restuarantAdministrator" => $restuarantAdministrator
		],200
		);	
		}else{
			return response()->json([
		"msg"=>"User not authorised !",
		"restuarantAdministrator" => $restuarantAdministrator
		],403
		);	
		}
		
		
	}
}
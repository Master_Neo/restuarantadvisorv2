<?php

namespace App\Http\Controllers;

use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\RestaurantAdministrator;
class RestaurantAdminController extends Controller
{
	
	public function index(){
		
		$restaurantAdministrator = \App\Models\Users::paginate(1);
		

		
		return response()->json([
		"msg"=>"Successfully retrieved restaurants admin!",
		"restaurantAdministrator" =>$restaurantAdministrator->toArray()		
		],200
		);	
	}	
	public function show(Resquest $request, $id){
		
		$restaurantAdministrator = \App\Models\Users::find($id);
		
		return response()->json([
		"msg"=>"Success!",
		"restaurantAdministrator" => $restaurantAdministrator
		],200
		);		
	
	}
	public function store(Request $request){
			
		$Administrator = new \App\Models\Users();
		
		$Administrator->tel = $request->tel;		
		$Administrator->email = $request->email;
		$Administrator->address = $request->address;		
		$Administrator->username  = $request->username;		
		$Administrator->password  = Hash::make($request->password);
		$Administrator->fullname  = $request->fullname;
		
		$Administrator->save();
		
		return response()->json([
		"msg"=>"Successfully saved restaurant Administrator!"
		],200
		);
	}
	
	public function update(Request $request, $id){
		
		$restuarantAdministrator =  \App\Models\Users::find($id);
		
		$restaurantAdministrator->cell = $request->cell;		
		$restaurantAdministrator->email = $request->email;
		$restaurantAdministrator->address = $request->address;		
		$restaurantAdministrator->username  = $request->username;		
		$restaurantAdministrator->password  = $request->password;
		$restaurantAdministrator->fullname  = $request->fullname;
		
		$restuarantAdministrator->save();
		
		return response()->json([
		"msg"=>"Successfully saved restaurant admin!",
		"restuarantAdministrator" => $restuarantAdministrator
		],200
		);	
	}
	
	public function destroy(Request $request, $id){
		
		$restuarantAdministrator = \App\Models\Users::find($id);
		
		$restuarantAdministrator->delete();
		return response()->json([
		"msg"=>"Successfully deleted restaurant admin!"
		],200
		);	
	}
	public function auth_admin(Request $request){
		
		$id = Auth::id();
			$user = Auth::user();
		if(Auth::attempt(['password'=>$request->password,'email' => $request->email])){
			return response()->json([
		"msg"=>"Successfully logged In !",
		"id" => $id,
		"User" => $user
		],200
		);
			
				
		}
		return response()->json([
			"msg"=>"Error login !"
			],403
			);
			
	}
}
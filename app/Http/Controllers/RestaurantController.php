<?php

namespace App\Http\Controllers;

//use Request;
use \App\Models\Restaurant;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
	
	public function index(){	
		
		$restaurant = \App\Models\Restaurant::paginate(1);
		$markers = \App\Models\Restaurant::get();
		
		return response()->json([
		"msg"=>"Successfully retrieved restaurants!",
		"restaurants" =>$restaurant->toArray(),
		"markers" => $markers
		],200
		);
	}
	
	public function show(Request $request, $id){		
		
		$restaurant = \App\Models\Restaurant::find($id);		
		return response()->json([
		"msg"=>"Successfully retrieved restaurant info!",
		"restaurant" =>$restaurant		
		],200
		);	
	}
	public function store(Request $request){
		
		$restaurant =  new \App\Models\Restaurant();		
		$restaurant->name  = $request->name; 
		$restaurant->address = $request->address;
		$restaurant->email = $request->email;
		$restaurant->tel = $request->tel;
		$restaurant->latitude =  $request->latitude;
		$restaurant->longitude =  $request->longitude;		
		$restaurant->user_id =  $request->user_id;		
		$restaurant->save();
		
		return response()->json([
		"msg"=>"Successfully saved restaurant!"
		],200
		);
	}
	
	public function update(Request $request, $id){
		
		$restaurant = \App\Models\Restaurant::find($id);	 
		$restaurant->address = $request->address;
		$restaurant->email = $request->email;
		$restaurant->tel = $request->tel;
		$restaurant->latitude =  $request->latitude;
		$restaurant->longitude =  $request->longitude;
		$restaurant->name  = $request->name;
		$restaurant->save();
		
		return response()->json([
		"msg"=>"Successfully edited and saved restaurant!"
		],200
		);
	}
	
	public function destroy(Request $request, $id){
		
		$restaurant = \App\Models\Restaurant::find($id);
		$restaurant->delete();
		return response()->json([
		"msg"=>"Successfully deleted restaurant!"
		],200
		);
	}
	
	}

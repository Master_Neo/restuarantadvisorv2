<?php

namespace App\Models;


use \Illuminate\Database\Eloquent\Model;

class RestaurantsAdministrators extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'email', 'password','email','address','tel'
    ];

	function Restaurant(){
		return this.hasMany('\App\Models\Restaurant');
		} 

}

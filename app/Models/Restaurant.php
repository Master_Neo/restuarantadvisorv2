<?php

namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','email','address','tel','latitude','longitude','user_id'
    ];
	
		function users(){
		return this.belongsTo('\App\Models\Users','user_id','user_id');
		} 
	}

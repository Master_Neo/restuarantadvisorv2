<?php

namespace App\Models;


use \Illuminate\Database\Eloquent\Model;

class RestaurantAdministrator extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'email', 'password','email','address','tel'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	public function Restaurant(){
		
		return $this->HasMany('\App\Models\Restaurant', 'RestaurantId','RestaurantId');
	}
}

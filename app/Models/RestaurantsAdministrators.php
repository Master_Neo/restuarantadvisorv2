<?php

namespace App\Models;


use \Illuminate\Database\Eloquent\Model;

class Administrators extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'email', 'password','email','address','tel'
    ];

	function restaurant(){
		return this.hasMany('\App\Models\Restaurant');
		} 

}
